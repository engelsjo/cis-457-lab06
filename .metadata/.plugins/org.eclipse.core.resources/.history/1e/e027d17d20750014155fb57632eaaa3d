'''
Client application for requesting a file
'''

import socket
import struct
import math

class Client(object):
    
    def __init__(self, port=3490, ipToSend=''):
        self.port = port
        self.ipToSend = ipToSend
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.socket.bind(('',self.port))
        self.fileSize = 0
        self.socket.settimeout(6)
        
        
    def requestFiles(self):
        running = True
        while running:
            try:
                fileRequested = raw_input("Please enter file to request: ")
                fileNameAsHex = [elem.encode("hex") for elem in fileRequested]
                hexString = self.getHexFromHex(fileNameAsHex)
                hexString += (' ' + '00') #if file request send zeros
                checkSum = str(self.getCheckSum(hexString))
                checkSum = checkSum[2:]
                fileNameAsHex.append('00')#request type first
                fileNameAsHex.append(checkSum)#checksum last
                fileRequestAsStr = ''
                for i in range(len(fileNameAsHex)):
                    fileRequestAsStr += (fileNameAsHex[i] + ' ')
                fileRequestAsStr = fileRequestAsStr.strip()
                print('Requesting file {}'.format(fileRequestAsStr))
                self.socket.sendto(fileRequestAsStr, (self.ipToSend, self.port)) #send request for file
                self.fileSize = self.getFileSize()
                windows = self.getWindowSizes(self.fileSize)
                with open(fileRequested, "wb") as out_file:
                    recvBuffer = []
                    for window in windows:
                        recvBuffer = self.recvPackets(window, recvBuffer, (self.ipToSend, self.port))
                        recvBuffer = self.orderPacketsCorrectly(window, recvBuffer)
                        for pkt in recvBuffer:
                            if pkt != '':
                                lenOfPktData = len(pkt) - 3
                                out_file.write(pkt[:lenOfPktData]) #dont write checksum bytes and so on...
                                out_file.flush()
                        recvBuffer = []
                       
            except KeyboardInterrupt:
                running = False
                break
            
    def sendAckForPacket(self, candidate, serverAddress):
        candidateAsList = [elem.encode("hex") for elem in candidate]
        candidateSeqNumAsInt = int(candidateAsList[len(candidate)-2],16)
        responseType = 1
        currCheck = 0
        candidateAckMsg = struct.pack('!BBB', responseType, candidateSeqNumAsInt, currCheck)
        hexString = self.getHexStringFromBytes(candidateAckMsg)
        candidateCheckSum = int(self.getCheckSum(hexString),16)
        candidateAckMsg = struct.pack('!BBB', responseType, candidateSeqNumAsInt, candidateCheckSum)
        print('Sending Ack message to server')
        self.socket.sendto(candidateAckMsg, serverAddress)
                
    def orderPacketsCorrectly(self, windowSize, recvBuffer):
        '''
        Returns our window in the correct order
        '''
        originalOrder = recvBuffer
        newOrder = ['' for elem in range(windowSize)]
        for pkt in originalOrder:
            pktAsHexList = [elem.encode("hex") for elem in pkt]
            seqNum = pktAsHexList[len(pktAsHexList)-2]
            newOrder[int(seqNum, 16)] = pkt
        return newOrder
                
    def recvPackets(self, currWindowSize, recvBuffer, serverAddress):
        '''
        Method ensures that window of 5 is all recieved...
        '''
        while True: #run until recv buffer is all full
            try:
                data, serverAddress = self.socket.recvfrom(self.port)
                print('Recieving data packet')
                hexString = self.getHexStringFromBytes(data)
                pktCheckSum = int(self.getCheckSum(hexString), 16)
                hexVals = [elem.encode("hex") for elem in data]
                
                type = int(hexVals[len(hexVals)-3], 16)
                
                if pktCheckSum == 0 and type == 0: #check sum is correct
                    if not self.verifyNoDupePacket(data, recvBuffer):
                        if len(recBuffer) == currWindowSize:
                            return recvBuffer
                        else:
                            self.sendAckForPacket(data, serverAddress)
                            recvBuffer.append(data)
                    else: #duplicate packet sent for window
                        self.sendAckForPacket(data, serverAddress)
            except socket.timeout: #return buffer when server has nothing left to say
                return recvBuffer

    
    def getFileSize(self):
        fileSize = 0
        while True: #keep trying to get file size
            data, serverAddress = self.socket.recvfrom(self.port)
            hexString = self.getHexStringFromBytes(data)
            checkSum = int(self.getCheckSum(hexString), 16)
            if checkSum == 0: #we got the write file size
                data = data[1:-1] #remove checksum and type byte
                dataAsList = [elem.encode("hex") for elem in data]
                hexValue = ''.join(elem for elem in dataAsList)
                fileSize = int(hexValue, 16)
                self.socket.sendto('ACK', serverAddress)
                return fileSize
            
    def getWindowSizes(self, fileSize):
        '''
        return a list of window sizes
        '''
        windows = []
        nbrOfFullWindows = fileSize/5120 #1024*5
        for window in range(nbrOfFullWindows):
            windows.append(5)
        leftOverData = fileSize%5120
        windowSize = int(math.ceil(leftOverData/1024.0)) #add one for empty string packet
        windows.append(windowSize)
        return windows  
        
    def getHexStringFromBytes(self, bytes):
        hexVals = [elem.encode("hex") for elem in bytes]
        hexString = ''
        for i in range(len(hexVals)):
            hex = str(hexVals[i])
            hexString += (hex + ' ')
        return hexString.strip()
    
    def getHexFromHex(self, bytes):
        hexString = ''
        for i in range(len(bytes)):
            hex = str(bytes[i])
            hexString += (hex + ' ')
        return hexString.strip()
    
    def getCheckSum(self, hexString):
        '''
        Method returns a check sum for a packet
        '''
        hexSections = hexString.split()
        result = 0
        for piece in hexSections:
            intPiece = int(piece, 16)
            result += intPiece
            binResult = bin(result)
            if len(binResult) == 11:
                result -= 256
                result += 1
        binResult = bin(result)[2:]
        while(len(binResult) < 8):
            binResult = ('0' + binResult)
        onesComp = ''
        for bit in binResult:
            if bit == '0':
                onesComp += '1'
            if bit == '1':
                onesComp += '0'
        finalByte = int(onesComp, 2)
        hexValue = hex(finalByte)
        
        return hexValue
    
    def verifyNoDupePacket(self, pkt, recvBuffer):
        pktAsList = [elem.encode("hex") for elem in pkt]
        pktSeqNum = int(pktAsList[len(pktAsList)-2],16)
        for recPkt in recvBuffer:
            hexList = [elem.encode("hex") for elem in recvBuffer]
            seqNum = int(hexList[len(hexList)-2],16)
            if pktSeqNum == seqNum:
                return True
        return False
        
if __name__ == "__main__":
    serverIP = raw_input("Please enter server ip address: ")
    serverPort = raw_input("Please enter port server is on: ")
    Client(int(serverPort), serverIP).requestFiles()